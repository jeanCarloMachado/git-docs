
Links
-----

 - [Referência oficial](https://git-scm.com/docs)
 - [Melhor tutorial de Git](http://rypress.com/tutorials/git/index)
 - [Encontrando issues no gitub](http://www.jeancarlomachado.com.br/blog/findingissuesongithub.html)
 - [Git para cientistas da computação](http://eagain.net/articles/git-for-computer-scientists/)

Livros
------
 - Git Pro (o melhor)
 - Git - Pragmatic bookshelf
