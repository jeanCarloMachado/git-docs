
Estudando fluxos de projetos open-source
----------------------------------------


- master
- develop
- topic




### Git

 - Mailing list + patches

### Kernel

 - Mailing list + patches
 - Mantenedores de sub-sistemas

### PHP

 - Github
 - Pull-requests
 - [Contributing](https://github.com/php/php-src/blob/master/CONTRIBUTING.md)


### Node

 - Github
 - Pull-requests
 - [Contributing](https://github.com/nodejs/node/blob/master/CONTRIBUTING.md)
 - [Collaborator Guide](https://github.com/nodejs/node/blob/master/COLLABORATOR_GUIDE.md)
